# ReactMarkdownEditor
Markdown Editor written using React/Redux

Demo here: https://samuelts.com/ReactMarkdownEditor

# Images
![main_view](https://raw.githubusercontent.com/safwyls/logos/master/md_editor.png)
![menu_view](https://raw.githubusercontent.com/safwyls/logos/master/md_editor_menu.png)
