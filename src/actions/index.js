export const PASS_MD = 'PASS_MD';
export const OPEN_MENU = 'OPEN_MENU';

export function PassMd(markdown) {
  return {
    type: PASS_MD,
    markdown
  }
}

export function OpenMenu(target) {
  return {
    type: OPEN_MENU,
    target
  }
}