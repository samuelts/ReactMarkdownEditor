import React from 'react';
import Header from './header';
import Menu from '../containers/menu';
import MarkdownField from '../containers/markdown_field';
import OutputField from '../containers/output_field';

const App = () => {
  return (
    <div id='wrapper'>
      <Header />
      <div id='appWrap'>
        <div id='fieldWrap'>
          <MarkdownField />
          <OutputField />
        </div>
        <Menu />
      </div>
    </div>
  );
}

export default App;