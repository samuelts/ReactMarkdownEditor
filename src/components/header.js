import React from 'react';
import MenuButton from '../containers/menu_button';

const Header = () => {
  return (
    <div id='headerWrap'>
      <h3 id='title'><i class='fab fa-markdown'></i>|editor</h3>
      <MenuButton />
    </div>
  );
}

export default Header;