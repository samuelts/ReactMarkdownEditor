export const INITIAL_STATE =
`# header1
## header2
### header3
#### header4
##### header5
###### header6

__bold__

_italic_

**_bold italic_**

* Lists
* are
* fun


1. Ordered lists
2. are also
3. fun

[In-line style link](http://www.samuelts.com)

[In-line style link with title](http://www.samuelts.com "Samuel's Portfolio")

inline code  \`coffee?happy:sad\`

\`\`\`
const rootReducer = combineReducers({
  markdown: MarkdownReducer
});
\`\`\`

> Blockquote

![Oregon Javascript!](https://raw.githubusercontent.com/safwyls/logos/master/oregon_js_small.png)`;