import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { PassMd } from '../actions';

class MarkdownField extends Component {
  constructor(props) {
    super(props);

    this.onInputChange = this.onInputChange.bind(this);
  }

  onInputChange(event) {
    this.props.markdown = event.target.value;
    this.props.PassMd(this.props.markdown);
  }

  render() {
    return (
      <textarea id='editor'
        name='editor'
        wrap='soft'
        value={this.props.markdown}
        onChange={this.onInputChange}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    markdown: state.markdown
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ PassMd }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MarkdownField);