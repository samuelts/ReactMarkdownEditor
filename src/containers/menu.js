import React, { Component } from 'react';
import { connect } from 'react-redux';

class Menu extends Component {

  render () {
    const menuState = this.props.menuIsOpen ? 'open' : 'closed';
    const toolList = [
      {name: 'React',
        link: 'https://reactjs.org',
        img: 'react'},
      {name: 'Redux',
        link: 'https://redux.js.org',
        img: 'redux'},
      {name: 'Webpack',
        link: 'https://webpack.js.org',
        img: 'webpack'},
      {name: 'Scss',
        link: 'https://sass-lang.com',
        img: 'sass'},
    ];

    const contactList = [
      {name: 'LinkedIn',
        link: 'https://www.linkedin.com/in/stephsam/',
        img: 'linkedin'},
      {name: 'Github',
        link: 'https://github.com/safwyls',
        img: 'github'},
      {name: 'Gmail',
        link: 'mailto:samueltstephenson@gmail.com',
        img: 'gmail'},
    ];

    const tools = toolList.map(tool => {
      return (
        <div class='tool'>
          <a href={tool.link} target='_blank' class='toolLink'>
            <img height='32' width='32' src={`https://unpkg.com/simple-icons@latest/icons/${tool.img}.svg`} />
          </a>
          {tool.name}
        </div>
      );
    });

    const contactLinks = contactList.map(link => {
      return (
        <a href={link.link} target='_blank' class='contactDetails'>
          <img src={`https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/${link.img}.svg`} alt={link.name} />
        </a>
      );
    })

    return (
      <div id='menu' className={menuState}>
        <div id='menuContent'>
          Created by Samuel Stephenson<br/>
          utilizing
          <div id='toolBox'>
            {tools}
          </div>
          <a href='https://samuelts.com'>More</a>
          <div id="contactLinks">
            {contactLinks}
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    menuIsOpen: state.menuIsOpen
  };
}

export default connect(mapStateToProps)(Menu);