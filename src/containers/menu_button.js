import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { OpenMenu } from '../actions';

class MenuButton extends Component {
  btnClick(event) {
    this.props.OpenMenu(event.target);
  }

  render() {
    const menuState = this.props.menuIsOpen ? 'open' : 'closed' ;
    return (
      <button className={menuState} id='menuBtn' onClick={this.btnClick.bind(this)}>
        <i class="fas fa-bars icon-closed"></i>
        <i class="fas fa-times icon-open"></i>
      </button>
    );
  }
}

function mapStateToProps(state) {
  return {
    menuIsOpen: state.menuIsOpen
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ OpenMenu }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuButton);