import React, { Component } from 'react';
import { connect } from 'react-redux';
import Markdown from 'markdown-to-jsx';

const MyLink = ({ children, ...props }) => (
  <a {...props} target='_blank'>{children}</a>
);

class OutputField extends Component {

  render() {
    if (!this.props.markdown) {
      return <div id='preview'></div>;
    }
    const markup = <Markdown
                    options={{
                      overrides: {
                        a: {
                          component: MyLink,
                        },
                      }
                    }}>{this.props.markdown}</Markdown>;

    return (
      <div id='preview'>
        {markup}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    markdown: state.markdown
  };
}

export default connect(mapStateToProps)(OutputField);