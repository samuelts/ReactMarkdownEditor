import { combineReducers } from 'redux';
import MarkdownReducer from './reducer_markdown';
import MenuReducer from './reducer_menu';

const rootReducer = combineReducers({
  markdown: MarkdownReducer,
  menuIsOpen: MenuReducer
});

export default rootReducer;