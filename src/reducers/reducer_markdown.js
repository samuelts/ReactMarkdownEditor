import { PASS_MD } from '../actions';
import { INITIAL_STATE } from '../components/initial_state';

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case PASS_MD:
      return action.markdown;
    default:
      return state;
  }
}