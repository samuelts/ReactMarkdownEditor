import { OPEN_MENU } from '../actions';

export default function(state = false, action) {
  switch (action.type) {
    case OPEN_MENU:
      return !state;
    default:
      return state;
  }
}